/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import java.util.Scanner;

/**
 * Ejemplo tomado a partir de:
 * http://chuwiki.chuidiang.org/index.php?title=Generar_n%C3%BAmeros_aleatorios_en_Java
 *
 * @author madarme
 */
public class PruebaRandom {

    public static void main(String[] args) {

        //leer un número:
        System.out.println("Por favor digite cuantos números desea generar:");
        Scanner lector = new Scanner(System.in);
        int cuantasVeces = lector.nextInt();
        System.out.println("Por favor digite límite inicial:");
        int N = lector.nextInt();
        System.out.println("Por favor digite límite final:");
        int M = lector.nextInt();
        while (cuantasVeces-- > 0) {
            int valorEntero = (int) Math.floor(Math.random() * (N - M + 1) + M);  // Valor entre M y N, ambos incluidos.
            System.out.println("Valor generado:" + valorEntero);
        }

    }

}
