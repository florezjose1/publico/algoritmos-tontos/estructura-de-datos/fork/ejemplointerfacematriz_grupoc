/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Interface.IMatriz;
import Interface.IMatriz2;
import Negocio.*;

import java.util.Scanner;

/**
 *
 * @author flore
 */
public class PruebaNumeroRepetido {

    public static void main(String[] args) {
        try {
            System.out.println("Por favor digite datos de la matriz:");
            Scanner lector = new Scanner(System.in);
            System.out.println("Por favor digite cantidad de filas:");
            int filas = lector.nextInt();
            System.out.println("Por favor digite cantidad de columnas:");
            int cols = lector.nextInt();
            System.out.println("Por favor digite limiInicial de elementos aleatorios:");
            int ini = lector.nextInt();
            System.out.println("Por favor digite limifinal de elementos aleatorios:");
            int fin = lector.nextInt();

            MatrizEntero_Aleatoria myMatriz = new MatrizEntero_Aleatoria(filas, cols);
            myMatriz.crearMatriz(ini, fin);
            System.out.println("Mi matriz: \n" + myMatriz.toString());
            
            // MatrizEntero_Aleatoria
            //Forma 1
            System.out.println("MatrizEntero_Aleatoria (Forma 1) 'Mas repetido' : " + myMatriz.getMas_Se_Repite());
            //Forma 2
            IMatriz2 i_matriz = myMatriz;
            System.out.println("MatrizEntero_Aleatoria (Forma 2) 'Mas repetido' : " + i_matriz.getMas_Se_Repite());
            //Forma 3
            int total = ((IMatriz2) myMatriz).getMas_Se_Repite();
            System.out.println("MatrizEntero_Aleatoria (Forma 3) 'Mas repetido' : " + total);
            
            // Par_Numero
            Par_Numero par = new Par_Numero(5, 5);
            //Forma 1
            System.out.println("Par_Numero (Forma 1) 'Mas repetido' : " + par.getMas_Se_Repite());
            //Forma 2
            IMatriz2 i_par = par;
            System.out.println("Par_Numero (Forma 2) 'Mas repetido' : " + i_par.getMas_Se_Repite());
            //Forma 3
            int total_par = ((IMatriz2) par).getMas_Se_Repite();
            System.out.println("Par_Numero (Forma 3) 'Mas repetido' : " + total_par);
            
            
            // VectorNumero
            VectorNumero vector = new VectorNumero(10);
            System.out.println("Mi Vector: \n" + vector.toString());
            //Forma 1
            System.out.println("VectorNumero (Forma 1) 'Mas repetido' : " + vector.getMas_Se_Repite());
            //Forma 2
            IMatriz2 i_vector = vector;
            System.out.println("VectorNumero (Forma 2) 'Mas repetido' : " + i_vector.getMas_Se_Repite());
            //Forma 3
            int total_vector = ((IMatriz2) vector).getMas_Se_Repite();
            System.out.println("VectorNumero (Forma 3) 'Mas repetido' : " + total_vector);

        } catch (Exception ex) {
            System.err.println("Error:" + ex.getMessage());
        }
    }
}
