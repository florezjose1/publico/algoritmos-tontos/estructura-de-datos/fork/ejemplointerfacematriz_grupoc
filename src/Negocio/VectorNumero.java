/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Interface.IMatriz;
import Interface.IMatriz2;

/**
 *
 * @author madar
 */
public class VectorNumero implements IMatriz, IMatriz2 {

    int numeros[];

    public VectorNumero() {
    }

    public VectorNumero(int n) throws Exception {
        if (n <= 0) {
            throw new Exception("No se puede crear el vector:");
        }
        this.numeros = new int[n];

        this.crearNumeros(n);

    }

    private void crearNumeros(int n) {
        for (int i = 0; i < n; i++) {
            this.numeros[i] = (int) Math.floor(Math.random() * (0 - n + 1) + n);
        }
    }

    public int[] getNumeros() {
        return numeros;
    }

    public void setNumeros(int[] numeros) {
        this.numeros = numeros;
    }

    @Override
    public String toString() {

        if (this.numeros == null) {
            return ("Vector vacío");
        }
        String msg = "";
        for (int dato : this.numeros) {
            msg += dato + "\t";
        }

        return msg;

    }

    @Override
    public int getSumaTotal() {
        int t = 0;
        for (int dato : this.numeros) {
            t += dato;
        }

        return t;
    }

    @Override
    public int getMas_Se_Repite() {
        // Creo un vector temporal con el {N} máximo de posibilidades
        int[] vectorTemp = new int[this.numeros.length];
        
        for (int dato : this.numeros) {
            // Cada posibilidad la auto-incremento
            vectorTemp[dato]++;
        }

        int maxRepetido = 0;
        int posicion = 0;
        for (int i = 0; i < vectorTemp.length; i++) {
            if (vectorTemp[i] > maxRepetido) {
                // guardo la cantidad de {n} veces encontrado
                maxRepetido = vectorTemp[i];
                // capturo la posición para saber a quien le pertenece
                posicion = i;
            }
        }

        return posicion;
    }

}
