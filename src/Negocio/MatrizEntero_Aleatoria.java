/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;

import Interface.IMatriz;
import Interface.IMatriz2;

/**
 *
 * @author madar
 */
public class MatrizEntero_Aleatoria implements IMatriz, IMatriz2 {

    private int matriz[][];
    private int limitePositivo;
    private int limiteNegativo;

    /**
     * Constructor vacío
     */
    public MatrizEntero_Aleatoria() {
        //NO es necesario, sólo por definición:
        this.matriz = null;
    }

    /**
     * Crea una matriz cuadrada o rectangular
     *
     * @param n cantidad de filas
     * @param m cantidad de columnas
     */
    public MatrizEntero_Aleatoria(int n, int m) throws Exception {
        if (n <= 0 || m <= 0) {
            throw new Exception("No se puede crear matrices con tamaños negativos o cero");
        }
        this.matriz = new int[n][m];
    }

    /**
     * Método que crear una matriz aleatorio
     *
     * @param limInicial valor inicial de generación del aleatorio
     * @param limFinal valor final de generación del aleatorio
     */
    public void crearMatriz(int limInicial, int limFinal) throws Exception {
        if (limInicial >= limFinal) {
            throw new Exception("No se puede llenar la matriz, sus límites están fuera del intervalo");
        }

        this.limitePositivo = limFinal;
        this.limiteNegativo = limInicial;
        for (int i = 0; i < this.matriz.length; i++) {
            //matriz[..][...]
            for (int j = 0; j < this.matriz[i].length; j++) {
                this.matriz[i][j] = (int) Math.floor(Math.random() * (limInicial - limFinal + 1) + limFinal);
            }
        }
    }

    public String toString() {

        if (this.matriz == null) {
            return "Matriz vacía";
        }
        String msg = "";
        for (int fila_vector[] : this.matriz) {
            for (int dato_columna : fila_vector) {
                msg += dato_columna + "\t";
            }
            msg += "\n";
        }
        return msg;

    }

    @Override
    public int getSumaTotal() {
        if (this.matriz == null) {
            return 0;
        }

        int total = 0;
        for (int fila_vector[] : this.matriz) {
            for (int dato_columna : fila_vector) {
                total += dato_columna;
            }
        }

        return total;
    }

    @Override
    public int getMas_Se_Repite() {
        int menor = this.limiteNegativo;
        if(menor < 0) menor *= -1;
        // Creo un vector negativo hasta {N} por si se presenta el caso
        int [] vectorN = new int[menor+1];
        
        int mayor = this.limitePositivo;
        if(mayor < 0) mayor *= -1;
        // Creo un vector positivo hasta {N} como máximo valor de casos
        int [] vectorP = new int[mayor+1];
            
        for(int []matriz : this.matriz) {
            for(int i = 0; i<matriz.length; i++) {
                if(this.limiteNegativo < 0 && matriz[i] < 0) {
                    // Si es negativo lo almaceno en el vector negativo y lo guardo como un contador
                    // valor {N} convertido a positivo
                    vectorN[matriz[i]*-1] ++;
                } else {
                    // guardo valor vector[i] en vector en forma de contador
                    vectorP[matriz[i]] ++;
                }
            }
        }
        
        int maxRepetidoN = 0;
        int posicionN = 0;
        for (int i = 0;vectorN != null && i < vectorN.length; i++) {
            if (vectorN[i] > maxRepetidoN) {
                // guardo la cantidad de {n} veces encontrado
                maxRepetidoN = vectorN[i];
                // capturo la posición para saber a quien le pertenece
                posicionN = i;
            }
        }
        int maxRepetidoP = 0;
        int posicionP = 0;
        for (int i = 0; vectorP != null && i < vectorP.length; i++) {
            if (vectorP[i] > maxRepetidoP) {
                // guardo la cantidad de {n} veces encontrado
                maxRepetidoP = vectorP[i];
                // capturo la posición para saber a quien le pertenece
                posicionP = i;
            }
        }
        // si uno es mayor que el otro retorno, de ser negativo el dominante lo convierto a negativo siendo su valor original
        return posicionN > posicionP ? posicionN * -1 : posicionP;
    }

}
